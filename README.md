# sdv-hashage_crypto
![](./Images/appli.PNG)

## Enjeux et objectifs : 
	Il s'agit de réaliser un utilitaire graphique en mode local permettant de proposer :
	un ensemble de fonctions de hachage un outil de chiffrement AES
	et la transformation de fichiers (chiffrement/déchiffrement).
	L'objectif pédagogique de ce projet est de mettre en place des prodédures et des moyens 
	communs pour pouvoir travaillé sur un projet commun.
	
## Rôles :
	Cassandra Lapalus : Lead Développeuse cassandra.lapalus@chat-miaou.fr
	Martin Lebret : Développeur crypto martin.lebret@chat-miaou.fr
	Mathis Gréau : Développeur crypto mathis.greau@chat-miaou.fr
	
## Architecture et technologies :

## Chiffrement :
![](./Images/chiffrement.png)

## Déchiffrement :
![](./Images/dechiffrement.png)

## Versions : 
Python3 : 3.9.12

OpenSSL : 1.1.1n

Bibliothèques Python3 :

	- hashlib 20081119
	- sys
	- os
	- random
	- pyside2 5.15.2.1
	- csv



## Règles de nommage : 

Fonctions : debutSuiteFin() 

Variables : debutSuiteFin

Exemple :

		def fonctionAddition(premierNombre, deuxiemeNombre):
			nombreAdditionne = premierNombre + deuxiemeNombre
			return nombreAdditionne

## Module Hashage
Avancement : 

	Code fonctionnel pour un fichier
	
TODO :

	Vérification des inputs (éviter les injections) : 0%

	Création de la base de données : 0%

	Connexion à la base de données : 0%
 
	Gestion des sels : 0%

	Génération de 256 bits réellement aléatoires (actuellement ils sont pseudo aléatoires) : 0%


## Modules Chiffrement et déchiffrement
Avancement : 

	Code fonctionnel pour un fichier

TODO :

	Vérification des inputs (éviter les injections) : 0%

	Création de la base de données : 0%

	Connexion à la base de données : 0%

	Gestion des clefs AES : 0%
	
## Interface GUI
Avancement : 

	Possibilité de chiffrer et déchiffrer un fichier et d'obtenir son hash salé. Les clefs  et les sels sont stockés dans un fichier chiffré.

TODO :

	Vérification des inputs (éviter les injections) : 0%

	Création de la base de données : 0%

	Stocker les clefs de chiffrement dans une BDD : 0%

	Stocker les sels dans une BDD : 0%

	Générer les clefs/sels lorsque l'ont n'est pas dans le fichier CSV : 0%

## Ressources communes 
- [ ] README.md
- [ ] Trello 

## Canal de communication 
	Utiliser le Discord du groupe

## Points particuliers et risques
- [ ] Il faut faire attention à la gestion des clefs privées et des secrets.

## Définition Ready

- [ ] Utiliser Visual Studio Code pour tous les développements Python
- [ ] Ne pas travailler à deux (ou plus) sur la même branche en même temps
- [ ] Lors de la création d'une branche s'assurer que les fichiers sonar-project.properties et .gitlab-ci.yml sont présents pour que SonarCloud analyse tous les push effectués dessus.
- [ ] Vérifier la place du module à développer dans l'architecture, les dépendances.
- [ ] Identifier par qui et comment le module va être utilisé.
- [ ] Identifier et entrées et sorties du module.
- [ ] Déterminer les jeux de test.

## Définition Done 

- [ ] Code à l'état fonctionnel 
- [ ] Git push effectué
- [ ] Tests unitaires réalisés et passés à 100% / SonarCloud
- [ ] Commenter les parties non triviales du code
- [ ] Code relu par un autre développeur
- [ ] Mettre à jour ou créer la documentation
- [ ] Avant de merge avec le main : s'assurer que le DONE est complété et avoir l'aval du lead Developper.euse




