# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'untitledTeGnbF.ui'
##
## Created by: Qt User Interface Compiler version 5.15.2
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################
import sys
import os
import hashlib
import random
from PySide2.QtCore import *
from PySide2.QtGui import *
from PySide2.QtWidgets import *
import csv



def hasher(file, md5Radio, sha1Radio,textEditId):  
    myTextEditId = textEditId.toPlainText()
    with open('info.csv', 'r', newline = '') as csv2: #Lecture du csv
        reader = csv.reader(csv2,delimiter  = ',')
        for row in reader:
            temp=row[0] #Identifiant
            salt=row[1] #Sel
            if temp == myTextEditId: 
                BLOCKSIZE = 65536
                if (sha1Radio.isChecked()): #Verification choix alog hashage
                    hasher = hashlib.sha1()
                elif (md5Radio.isChecked()):
                    hasher = hashlib.md5()    
                with open(file, 'rb') as afile: #hashage fichier
                    buf = afile.read(BLOCKSIZE)
                    hasher.update(buf)
                    buf = salt.encode('utf-8')
                    encoding =hasher.hexdigest()
                    while len(buf) > 0:
                        hasher.update(buf)
                        buf = afile.read(BLOCKSIZE)
                encoding =hasher.hexdigest()
            else:
                print("Utilisateur non trouvé")
    return encoding
    
def dechiffrerCsv(fichier, clef):
    os.system('openssl enc -d -aes-256-cbc -salt -pbkdf2 -in '+fichier+'  -out "'+"info.csv"+'"  -pass file:'+clef+'')   


def chiffrer(textEditFichier, textEditClef, textBrowser, md5Radio, sha1Radio, textEditId):
    myTextFichier = textEditFichier.toPlainText()
    myTextClef = textEditClef.toPlainText()
    dechiffrerCsv("info.csv.miaou","key.bin") #dechiffrement fichier csv
    hashFichier = hasher(myTextFichier, md5Radio, sha1Radio, textEditId)
    mytextEditId = textEditId.toPlainText()
    with open('info.csv', 'r', newline = '') as csv2: #Lecture du csv
        reader = csv.reader(csv2,delimiter  = ',')
        for row in reader:
            temp=row[0] #identifiant
            key=row[2] #Clef AES256
            if temp == mytextEditId:
                textBrowser.append("Hash fichier : "+hashFichier)
                os.system('openssl enc -aes-256-cbc -salt -pbkdf2 -in "'+myTextFichier+'" -out "'+myTextFichier+'.miaou"  -K "'+key+'" -iv 0')
                textBrowser.append("Fichier chiffré : "+myTextFichier+'.miaou')
    os.remove("info.csv")

def dechiffrer(textEditFichier, textEditClef, textBrowser, md5Radio, sha1Radio, textEditId):
    myTextFichierChiffre = textEditFichier.toPlainText()
    myTextFichierDechiffre =  myTextFichierChiffre[:- 6]
    myTextClef = textEditClef.toPlainText()
    mytextEditId = textEditId.toPlainText()
    dechiffrerCsv("info.csv.miaou","key.bin")
    with open('info.csv', 'r', newline = '') as csv2: #Lecture du csv
        reader = csv.reader(csv2,delimiter  = ',')
        for row in reader:
            temp=row[0] #identifiant
            key=row[2] # clef AES256
            if temp == mytextEditId:    
                os.system('openssl enc -d -aes-256-cbc -salt -pbkdf2 -in "'+myTextFichierChiffre+'"  -out "'+myTextFichierDechiffre+'"  -K "'+key+'" -iv 0')    
                textBrowser.append("Fichier déchiffré : "+myTextFichierDechiffre)
                hashFichier = hasher(myTextFichierDechiffre, md5Radio, sha1Radio, textEditId)
                textBrowser.append("Hash fichier déchiffré : "+hashFichier)
    os.remove("info.csv")
        
def getValue(textEdit):
    mytext = textEdit.toPlainText()
    print(mytext)
    
def chercherFichier(textEdit):
    fileName=QFileDialog.getOpenFileName()
    textEdit.setText(fileName[0])
           

class Ui_MainWindow(object):
    
        
    def setupUi(self, MainWindow):
        if not MainWindow.objectName():
            MainWindow.setObjectName(u"MainWindow")
        MainWindow.resize(800, 600)
        self.centralwidget = QWidget(MainWindow)
        self.centralwidget.setObjectName(u"centralwidget")
        
        self.textEdit = QTextEdit(self.centralwidget)
        self.textEdit.setObjectName(u"textEdit")
        self.textEdit.setGeometry(QRect(30, 40, 331, 41))
        
        self.textEdit_2 = QTextEdit(self.centralwidget)
        self.textEdit_2.setObjectName(u"textEdit_2")
        self.textEdit_2.setGeometry(QRect(430, 40, 331, 41))
        
        self.textBrowser = QTextBrowser(self.centralwidget)
        self.textBrowser.setObjectName(u"textBrowser")
        self.textBrowser.setGeometry(QRect(35, 211, 401, 201))
        
        self.pushButton = QPushButton(self.centralwidget)
        self.pushButton.setObjectName(u"pushButton")
        self.pushButton.setGeometry(QRect(40, 450, 191, 71))
        self.pushButton.setAutoDefault(False)
        self.pushButton.clicked.connect(lambda: chiffrer(self.textEdit, self.textEdit_2, self.textBrowser, self.radioButton, self.radioButton_2, self.textEdit_3))
        
        self.pushButton_2 = QPushButton(self.centralwidget)
        self.pushButton_2.setObjectName(u"pushButton_2")
        self.pushButton_2.setGeometry(QRect(570, 450, 191, 71))
        self.pushButton_2.clicked.connect(lambda: dechiffrer(self.textEdit, self.textEdit_2, self.textBrowser, self.radioButton, self.radioButton_2, self.textEdit_3))

        self.textEdit_3 = QTextEdit(self.centralwidget)

        self.textEdit_3.setObjectName(u"textEdit_3")

        self.textEdit_3.setGeometry(QRect(420, 120, 341, 41))

        self.label_4 = QLabel(self.centralwidget)

        self.label_4.setObjectName(u"label_4")

        self.label_4.setGeometry(QRect(335, 130, 81, 20))
        
        self.pushButton_3 = QPushButton(self.centralwidget)
        self.pushButton_3.setObjectName(u"pushButton_3")
        self.pushButton_3.setGeometry(QRect(140, 10, 87, 27))
        self.pushButton_3.clicked.connect(lambda: chercherFichier(self.textEdit))
        
        self.pushButton_4 = QPushButton(self.centralwidget)
        self.pushButton_4.setObjectName(u"pushButton_4")
        self.pushButton_4.setGeometry(QRect(520, 10, 87, 27))
        self.pushButton_4.clicked.connect(lambda: chercherFichier(self.textEdit_2))
          
        self.label = QLabel(self.centralwidget)
        self.label.setObjectName(u"label")
        self.label.setGeometry(QRect(30, 10, 111, 19))
        self.radioButton = QRadioButton(self.centralwidget)
        self.radioButton.setObjectName(u"radioButton")
        self.radioButton.setGeometry(QRect(60, 140, 109, 25))
        self.radioButton_2 = QRadioButton(self.centralwidget)
        self.radioButton_2.setObjectName(u"radioButton_2")
        self.radioButton_2.setGeometry(QRect(60, 160, 109, 25))
        self.label_2 = QLabel(self.centralwidget)
        self.label_2.setObjectName(u"label_2")
        self.label_2.setGeometry(QRect(50, 110, 181, 19))
        self.label_3 = QLabel(self.centralwidget)
        self.label_3.setObjectName(u"label_3")
        self.label_3.setGeometry(QRect(430, 10, 111, 19))
        
        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QMenuBar(MainWindow)
        self.menubar.setObjectName(u"menubar")
        self.menubar.setGeometry(QRect(0, 0, 800, 24))
        MainWindow.setMenuBar(self.menubar)
        self.statusbar = QStatusBar(MainWindow)
        self.statusbar.setObjectName(u"statusbar")
        MainWindow.setStatusBar(self.statusbar)

        self.retranslateUi(MainWindow)

        self.pushButton.setDefault(False)
	 

        QMetaObject.connectSlotsByName(MainWindow)
    # setupUi
    
    def retranslateUi(self, MainWindow):
        MainWindow.setWindowTitle(QCoreApplication.translate("MainWindow", u"MainWindow", None))
        self.pushButton.setText(QCoreApplication.translate("MainWindow", u"Chiffrer", None))
        self.pushButton_2.setText(QCoreApplication.translate("MainWindow", u"Dechiffrer", None))
        self.label.setText(QCoreApplication.translate("MainWindow", u"Chemin fichier", None))
        self.radioButton.setText(QCoreApplication.translate("MainWindow", u"md5", None))
        self.radioButton_2.setText(QCoreApplication.translate("MainWindow", u"sha1", None))
        self.label_2.setText(QCoreApplication.translate("MainWindow", u"Algorithme hashage", None))
        self.label_3.setText(QCoreApplication.translate("MainWindow", u"Chemin clef", None))
        self.pushButton_3.setText(QCoreApplication.translate("MainWindow", u"Choisir", None))
        self.pushButton_4.setText(QCoreApplication.translate("MainWindow", u"Choisir", None))
        self.label_4.setText(QCoreApplication.translate("MainWindow", u"Identifiant :", None))
    # retranslateUi
    
    
if __name__ == "__main__":  
    app = QApplication(sys.argv)
# Create a Qt widget, which will be our window
    w = QMainWindow()
    w.show()
    window = Ui_MainWindow()
    window.setupUi(w)
    sys.exit(app.exec_())
    

